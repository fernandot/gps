unsigned char k;
unsigned char userWR_buffer[64], userRD_buffer[64];
char uart_rd[200];


//**************************************************************************************************
// Main Interrupt Routine
//**************************************************************************************************
void interrupt()
{
  HID_InterruptProc();

}
//**************************************************************************************************

//**************************************************************************************************
// Initialization Routine
//**************************************************************************************************

void Init_Main()
{
        //--------------------------------------
        // Disable all interrupts
        //--------------------------------------
        INTCON = 0;                             // Disable GIE, PEIE, TMR0IE,INT0IE,RBIE
        INTCON2 = 0xF5;
        INTCON3 = 0xC0;
        RCON.IPEN = 0;                          // Disable Priority Levels on interrupts
        PIE1 = 0;
        PIE2 = 0;
        PIR1 = 0;
        PIR2 = 0;
        
        //RCSTA = 0xB8;
        
                    /*
                    SYNC_bit = 0;
BRGH_bit = 1;
BRG16_bit = 0;*/

//OSCCON = 0x77;

        ADCON1 |= 0x0F;                         // Configure all ports with analog function as digital
        CMCON  |= 7;                            // Disable comparators
        //--------------------------------------
        // Ports Configuration
        //--------------------------------------



        TRISA = 0;
        TRISB = 0;
        TRISC = 0xFF;
        TRISD = 0;
        TRISE = 0x07;

        LATA = 0;
        LATB = 0;
        LATC = 0;
        LATD = 0;
        LATE = 0;
        //--------------------------------------
        // Clear user RAM
        // Banks [00 .. 07] ( 8 x 256 = 2048 Bytes )
        //--------------------------------------

}
//**************************************************************************************************

//**************************************************************************************************
// Main Program Routine
//**************************************************************************************************

void main() {
  unsigned char i, ch;
  char m[10];
  unsigned int c;

  Init_Main();

  HID_Enable(&userRD_buffer, &userWR_buffer);
  Delay_ms(1000);

  I2C1_Init(100000);
  Delay_100ms();
  
  UART1_Init(9600);
  Delay_100ms();

  while (1) {
    k = HID_Read();
    i = 0;
    if(k>0)
    {
        Delay_10ms();

    while (i < k) {
      ch = userRD_buffer[i];
      userWR_buffer[0] = ch;

      while (!HID_Write(&userWR_buffer, 1)) ;
      i++;
    }
   /*
    strcpy(m," Mem�ria:");
    for(c=0;c<10;c++)
    {
       userWR_buffer[0] = m[c];
       while (!HID_Write(&userWR_buffer, 1)) ;
    }

  if(!I2C1_Start())
  {
    I2C1_Wr(0xA0);          // send byte via I2C  (device address + W)
    I2C1_Wr(0x00); // Send byte (data address)
    I2C1_Wr('1');          // send data (data to be written)
    I2C1_Wr('2');          // send data (data to be written)
    I2C1_Wr('3');          // send data (data to be written)
    I2C1_Wr('4');          // send data (data to be written)
    I2C1_Wr('5');          // send data (data to be written)
    I2C1_Wr('6');          // send data (data to be written)
    I2C1_Wr('7');          // send data (data to be written)
    I2C1_Stop();            // issue I2C stop signal
  }

  Delay_100ms();

    if(!I2C1_Start())
    {
     I2C1_Wr(0xA0);          // send byte via I2C  (device address + W)
     I2C1_Wr(0x00); // Send byte (data address)
     I2C1_Repeated_Start();  // issue I2C signal repeated start
     I2C1_Wr(0xA1);          // send byte (device address + R)
     for(c=0;c<2047;c++)
     {
         ch = I2C1_Rd(1);    // Read the data (acknowledge)
        userWR_buffer[0] = ch;
        while (!HID_Write(&userWR_buffer, 1)) ;
        Delay_100ms();
     }
     ch = I2C1_Rd(0u);    // Read the data (NO acknowledge)
     userWR_buffer[0] = ch;
     while (!HID_Write(&userWR_buffer, 1)) ;
     I2C1_Stop();            // issue I2C stop signal
   }               */

    strcpy(m," GPS:");
    for(c=0;c<5;c++)
    {
       userWR_buffer[0] = m[c];
       while (!HID_Write(&userWR_buffer, 1)) ;
    }

  while (1) {                    // Endless loop
   if (UART1_Data_Ready()) {     // If data is received,
   memset(uart_rd,'\0',200);
   UART1_Read_Text(uart_rd,'\n',200);
     //uart_rd = ADC_Read(0);     //   read the received data,
    for(c=0;c<200;c++)
    {
       userWR_buffer[0] = uart_rd[c];
       while (!HID_Write(&userWR_buffer, 1)) ;
       Delay_10ms();
    }
       /*
    uart_rd[0]=   UART1_Read();
       userWR_buffer[0] = uart_rd[0];
       while (!HID_Write(&userWR_buffer, 1)) ;  */
    
    }
  }


    }
   }
    
  HID_Disable();
}
//**************************************************************************************************
