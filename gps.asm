
_interrupt:
;gps.c,9 :: 		void interrupt()
;gps.c,11 :: 		HID_InterruptProc();
	CALL        _HID_InterruptProc+0, 0
;gps.c,13 :: 		}
L__interrupt21:
	RETFIE      1
; end of _interrupt

_Init_Main:
;gps.c,20 :: 		void Init_Main()
;gps.c,25 :: 		INTCON = 0;                             // Disable GIE, PEIE, TMR0IE,INT0IE,RBIE
	CLRF        INTCON+0 
;gps.c,26 :: 		INTCON2 = 0xF5;
	MOVLW       245
	MOVWF       INTCON2+0 
;gps.c,27 :: 		INTCON3 = 0xC0;
	MOVLW       192
	MOVWF       INTCON3+0 
;gps.c,28 :: 		RCON.IPEN = 0;                          // Disable Priority Levels on interrupts
	BCF         RCON+0, 7 
;gps.c,29 :: 		PIE1 = 0;
	CLRF        PIE1+0 
;gps.c,30 :: 		PIE2 = 0;
	CLRF        PIE2+0 
;gps.c,31 :: 		PIR1 = 0;
	CLRF        PIR1+0 
;gps.c,32 :: 		PIR2 = 0;
	CLRF        PIR2+0 
;gps.c,43 :: 		ADCON1 |= 0x0F;                         // Configure all ports with analog function as digital
	MOVLW       15
	IORWF       ADCON1+0, 1 
;gps.c,44 :: 		CMCON  |= 7;                            // Disable comparators
	MOVLW       7
	IORWF       CMCON+0, 1 
;gps.c,51 :: 		TRISA = 0;
	CLRF        TRISA+0 
;gps.c,52 :: 		TRISB = 0;
	CLRF        TRISB+0 
;gps.c,53 :: 		TRISC = 0xFF;
	MOVLW       255
	MOVWF       TRISC+0 
;gps.c,54 :: 		TRISD = 0;
	CLRF        TRISD+0 
;gps.c,55 :: 		TRISE = 0x07;
	MOVLW       7
	MOVWF       TRISE+0 
;gps.c,57 :: 		LATA = 0;
	CLRF        LATA+0 
;gps.c,58 :: 		LATB = 0;
	CLRF        LATB+0 
;gps.c,59 :: 		LATC = 0;
	CLRF        LATC+0 
;gps.c,60 :: 		LATD = 0;
	CLRF        LATD+0 
;gps.c,61 :: 		LATE = 0;
	CLRF        LATE+0 
;gps.c,67 :: 		}
	RETURN      0
; end of _Init_Main

_main:
;gps.c,74 :: 		void main() {
;gps.c,79 :: 		Init_Main();
	CALL        _Init_Main+0, 0
;gps.c,81 :: 		HID_Enable(&userRD_buffer, &userWR_buffer);
	MOVLW       _userRD_buffer+0
	MOVWF       FARG_HID_Enable_ReadBuff+0 
	MOVLW       hi_addr(_userRD_buffer+0
	MOVWF       FARG_HID_Enable_ReadBuff+1 
	MOVLW       _userWR_buffer+0
	MOVWF       FARG_HID_Enable_WriteBuff+0 
	MOVLW       hi_addr(_userWR_buffer+0
	MOVWF       FARG_HID_Enable_WriteBuff+1 
	CALL        _HID_Enable+0, 0
;gps.c,82 :: 		Delay_ms(1000);
	MOVLW       26
	MOVWF       R11, 0
	MOVLW       94
	MOVWF       R12, 0
	MOVLW       110
	MOVWF       R13, 0
L_main0:
	DECFSZ      R13, 1, 0
	BRA         L_main0
	DECFSZ      R12, 1, 0
	BRA         L_main0
	DECFSZ      R11, 1, 0
	BRA         L_main0
	NOP
;gps.c,84 :: 		I2C1_Init(100000);
	MOVLW       50
	MOVWF       SSPADD+0 
	CALL        _I2C1_Init+0, 0
;gps.c,85 :: 		Delay_100ms();
	CALL        _Delay_100ms+0, 0
;gps.c,87 :: 		UART1_Init(9600);
	MOVLW       129
	MOVWF       SPBRG+0 
	BSF         TXSTA+0, 2, 0
	CALL        _UART1_Init+0, 0
;gps.c,88 :: 		Delay_100ms();
	CALL        _Delay_100ms+0, 0
;gps.c,90 :: 		while (1) {
L_main1:
;gps.c,91 :: 		k = HID_Read();
	CALL        _HID_Read+0, 0
	MOVF        R0, 0 
	MOVWF       _k+0 
;gps.c,92 :: 		i = 0;
	CLRF        main_i_L0+0 
;gps.c,93 :: 		if(k>0)
	MOVF        R0, 0 
	SUBLW       0
	BTFSC       STATUS+0, 0 
	GOTO        L_main3
;gps.c,95 :: 		Delay_10ms();
	CALL        _Delay_10ms+0, 0
;gps.c,97 :: 		while (i < k) {
L_main4:
	MOVF        _k+0, 0 
	SUBWF       main_i_L0+0, 0 
	BTFSC       STATUS+0, 0 
	GOTO        L_main5
;gps.c,98 :: 		ch = userRD_buffer[i];
	MOVLW       _userRD_buffer+0
	MOVWF       FSR0L 
	MOVLW       hi_addr(_userRD_buffer+0
	MOVWF       FSR0H 
	MOVF        main_i_L0+0, 0 
	ADDWF       FSR0L, 1 
	BTFSC       STATUS+0, 0 
	INCF        FSR0H, 1 
	MOVF        POSTINC0+0, 0 
	MOVWF       _userWR_buffer+0 
;gps.c,99 :: 		userWR_buffer[0] = ch;
;gps.c,101 :: 		while (!HID_Write(&userWR_buffer, 1)) ;
L_main6:
	MOVLW       _userWR_buffer+0
	MOVWF       FARG_HID_Write_Buffer+0 
	MOVLW       hi_addr(_userWR_buffer+0
	MOVWF       FARG_HID_Write_Buffer+1 
	MOVLW       1
	MOVWF       FARG_HID_Write_Len+0 
	CALL        _HID_Write+0, 0
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main7
	GOTO        L_main6
L_main7:
;gps.c,102 :: 		i++;
	INCF        main_i_L0+0, 1 
;gps.c,103 :: 		}
	GOTO        L_main4
L_main5:
;gps.c,147 :: 		strcpy(m," GPS:");
	MOVLW       main_m_L0+0
	MOVWF       FARG_strcpy_to+0 
	MOVLW       hi_addr(main_m_L0+0
	MOVWF       FARG_strcpy_to+1 
	MOVLW       ?lstr1_gps+0
	MOVWF       FARG_strcpy_from+0 
	MOVLW       hi_addr(?lstr1_gps+0
	MOVWF       FARG_strcpy_from+1 
	CALL        _strcpy+0, 0
;gps.c,148 :: 		for(c=0;c<5;c++)
	CLRF        main_c_L0+0 
	CLRF        main_c_L0+1 
L_main8:
	MOVLW       0
	SUBWF       main_c_L0+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main22
	MOVLW       5
	SUBWF       main_c_L0+0, 0 
L__main22:
	BTFSC       STATUS+0, 0 
	GOTO        L_main9
;gps.c,150 :: 		userWR_buffer[0] = m[c];
	MOVLW       main_m_L0+0
	ADDWF       main_c_L0+0, 0 
	MOVWF       FSR0L 
	MOVLW       hi_addr(main_m_L0+0
	ADDWFC      main_c_L0+1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	MOVWF       _userWR_buffer+0 
;gps.c,151 :: 		while (!HID_Write(&userWR_buffer, 1)) ;
L_main11:
	MOVLW       _userWR_buffer+0
	MOVWF       FARG_HID_Write_Buffer+0 
	MOVLW       hi_addr(_userWR_buffer+0
	MOVWF       FARG_HID_Write_Buffer+1 
	MOVLW       1
	MOVWF       FARG_HID_Write_Len+0 
	CALL        _HID_Write+0, 0
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main12
	GOTO        L_main11
L_main12:
;gps.c,148 :: 		for(c=0;c<5;c++)
	INFSNZ      main_c_L0+0, 1 
	INCF        main_c_L0+1, 1 
;gps.c,152 :: 		}
	GOTO        L_main8
L_main9:
;gps.c,154 :: 		while (1) {                    // Endless loop
L_main13:
;gps.c,155 :: 		if (UART1_Data_Ready()) {     // If data is received,
	CALL        _UART1_Data_Ready+0, 0
	MOVF        R0, 1 
	BTFSC       STATUS+0, 2 
	GOTO        L_main15
;gps.c,156 :: 		memset(uart_rd,'\0',200);
	MOVLW       _uart_rd+0
	MOVWF       FARG_memset_p1+0 
	MOVLW       hi_addr(_uart_rd+0
	MOVWF       FARG_memset_p1+1 
	CLRF        FARG_memset_character+0 
	MOVLW       200
	MOVWF       FARG_memset_n+0 
	MOVLW       0
	MOVWF       FARG_memset_n+1 
	CALL        _memset+0, 0
;gps.c,157 :: 		UART1_Read_Text(uart_rd,'\n',200);
	MOVLW       _uart_rd+0
	MOVWF       FARG_UART1_Read_Text_Output+0 
	MOVLW       hi_addr(_uart_rd+0
	MOVWF       FARG_UART1_Read_Text_Output+1 
	MOVLW       10
	MOVWF       FARG_UART1_Read_Text_Delimiter+0 
	MOVLW       0
	MOVWF       FARG_UART1_Read_Text_Delimiter+1 
	MOVLW       200
	MOVWF       FARG_UART1_Read_Text_Attempts+0 
	CALL        _UART1_Read_Text+0, 0
;gps.c,159 :: 		for(c=0;c<200;c++)
	CLRF        main_c_L0+0 
	CLRF        main_c_L0+1 
L_main16:
	MOVLW       0
	SUBWF       main_c_L0+1, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main23
	MOVLW       200
	SUBWF       main_c_L0+0, 0 
L__main23:
	BTFSC       STATUS+0, 0 
	GOTO        L_main17
;gps.c,161 :: 		userWR_buffer[0] = uart_rd[c];
	MOVLW       _uart_rd+0
	ADDWF       main_c_L0+0, 0 
	MOVWF       FSR0L 
	MOVLW       hi_addr(_uart_rd+0
	ADDWFC      main_c_L0+1, 0 
	MOVWF       FSR0H 
	MOVF        POSTINC0+0, 0 
	MOVWF       _userWR_buffer+0 
;gps.c,162 :: 		while (!HID_Write(&userWR_buffer, 1)) ;
L_main19:
	MOVLW       _userWR_buffer+0
	MOVWF       FARG_HID_Write_Buffer+0 
	MOVLW       hi_addr(_userWR_buffer+0
	MOVWF       FARG_HID_Write_Buffer+1 
	MOVLW       1
	MOVWF       FARG_HID_Write_Len+0 
	CALL        _HID_Write+0, 0
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main20
	GOTO        L_main19
L_main20:
;gps.c,163 :: 		Delay_10ms();
	CALL        _Delay_10ms+0, 0
;gps.c,159 :: 		for(c=0;c<200;c++)
	INFSNZ      main_c_L0+0, 1 
	INCF        main_c_L0+1, 1 
;gps.c,164 :: 		}
	GOTO        L_main16
L_main17:
;gps.c,170 :: 		}
L_main15:
;gps.c,171 :: 		}
	GOTO        L_main13
;gps.c,174 :: 		}
L_main3:
;gps.c,175 :: 		}
	GOTO        L_main1
;gps.c,178 :: 		}
	GOTO        $+0
; end of _main
