
_InitUSBdsc:
;USBdsc.c,206 :: 		void InitUSBdsc()
;USBdsc.c,208 :: 		Byte_tmp_0[0] = NUM_ENDPOINTS;
	MOVLW       2
	MOVWF       _Byte_tmp_0+0 
;USBdsc.c,209 :: 		Byte_tmp_0[0] = ConfigDescr_wTotalLength;
	MOVLW       41
	MOVWF       _Byte_tmp_0+0 
;USBdsc.c,210 :: 		Byte_tmp_0[0] = HID_ReportDesc_len;
	MOVLW       47
	MOVWF       _Byte_tmp_0+0 
;USBdsc.c,211 :: 		Byte_tmp_0[0] = Low_HID_ReportDesc_len;
	MOVLW       47
	MOVWF       _Byte_tmp_0+0 
;USBdsc.c,212 :: 		Byte_tmp_0[0] = High_HID_ReportDesc_len;
	CLRF        _Byte_tmp_0+0 
;USBdsc.c,213 :: 		Byte_tmp_0[0] = Low_HID_PACKET_SIZE;
	MOVLW       64
	MOVWF       _Byte_tmp_0+0 
;USBdsc.c,214 :: 		Byte_tmp_0[0] = High_HID_PACKET_SIZE;
	CLRF        _Byte_tmp_0+0 
;USBdsc.c,224 :: 		}
	RETURN      0
; end of _InitUSBdsc
